package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	//商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	//商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";


	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "定義ファイルが存在しません";
	private static final String FILE_INVALID_FORMAT = "定義ファイルのフォーマットが不正です";
	private static final String NOT_SERIAL_NUMBER_NAME = "売上ファイル名が連番になっていません";
	private static final String OVER_10_DIGITS = "合計金額が10桁を超えました";
	private static final String ILLEGAL_FORMAT = "のフォーマットが不正です";
	private static final String BRANCH_INVLID_CODE = "の支店コードが不正です";
	private static final String COMMODDITY_INVALID_CODE = "の商品コードが不正です";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {
		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();

		//商品コードと商品名を保持するMap
		Map<String, String> commodityNames = new HashMap<>();
		//商品コードと売上金額を保持するMap
		Map<String, Long> commoditySales = new HashMap<>();

		//正規表現構文
		String branchExpress = "^[0-9]*${3}";
		String commodityExpress = "^[0-9a-zA-Z]*${8}";

		//支店
		String branchWord = "支店";
		//商品
		String commodityWord = "商品";

		//エラー処理3-1
		//コマンドライン引数が設定されているか
		if(args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}

		// 支店定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, branchExpress, branchWord)) {
			return;
		}

		//商品定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_COMMODITY_LST,commodityNames, commoditySales, commodityExpress, commodityWord)){
			return;

		}

		// ※ここから集計処理を作成してください。(処理内容2-1、2-2)
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();

		for(int i = 0; i < files.length; i++) {
			//エラー処理3-2
			//ファイルなのか確認する
			if(files[i].isFile() && files[i].getName().matches("[0-9]{8}.rcd")) {

				rcdFiles.add(files[i]);
			}
		}

		//エラー処理2-1
		//比較回数は売上ファイルの数より１回少ない
		for(int i = 0; i < rcdFiles.size() - 1; i++) {

			//比較する２つの売上ファイル名の先頭から数字８文字を切り出し、int型に変換する
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));

			int latter = Integer.parseInt(rcdFiles.get(i+1).getName().substring(0,8));

			//２つのファイル名の数字を比較して、差が１出なかったら、eメッセージ
			if((latter - former) != 1) {
				System.out.println(NOT_SERIAL_NUMBER_NAME);
				return;

			}
		}

		BufferedReader br = null;

		for(int i = 0; i < rcdFiles.size(); i++) {

			try {
				FileReader fr = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(fr);

				String line;
				List<String> rcdDatas = new ArrayList<>();

				while((line = br.readLine()) != null) {
					rcdDatas.add(line);
				}
				//エラー処理2-4
				//売上ファイルの中身が３桁以上ある場合
				if(rcdDatas.size() != 3) {
					System.out.println(rcdFiles.get(i).getName()+ ILLEGAL_FORMAT);
					return;
				}

				//エラー処理2-3,2-4
				//支店(商品)に該当がなかった場合は、eメッセージ
				if(!branchNames.containsKey(rcdDatas.get(0))) {
					System.out.println(rcdFiles.get(i).getName() + BRANCH_INVLID_CODE);
					return;
				}

				if(!commodityNames.containsKey(rcdDatas.get(1))) {
					System.out.println(rcdFiles.get(i).getName() + COMMODDITY_INVALID_CODE);
					return;
				}

				//エラー処理3-3
				//売上金額が数字なのか
				if(!rcdDatas.get(2).matches("^[0-9]*$")){
					System.out.println(UNKNOWN_ERROR);
					return;
				}

				long fileSale = Long.parseLong(rcdDatas.get(2));

				Long salesAmount = branchSales.get(rcdDatas.get(0)) + fileSale;

				Long commoditySalesAmount = commoditySales.get(rcdDatas.get(1)) + fileSale;


				//エラー処理2-2
				//集計した売上金額が10桁を超えた場合、エラーメッセージ「合計金額が10桁を超えました」
				if(salesAmount >= 10000000000L) {
					System.out.println(OVER_10_DIGITS);
					return;
				}

				if(commoditySalesAmount  >= 10000000000L) {

					System.out.println(OVER_10_DIGITS);
					return;
				}

				branchSales.put((rcdDatas.get(0)), salesAmount);

				commoditySales.put((rcdDatas.get(1)),commoditySalesAmount);

			}
			catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;

			}finally {
				if(br != null) {
					try {
						br.close();

					}catch (IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}

			// 支店別集計ファイル書き込み処理
			if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
				return;
			}

			//商品別集計ファイル書き込み処理
			if(!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
				return;
			}
		}
	}

	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> names, Map<String, Long> sales, String express,String word) {

		//エラー処理1-1
		//確認したいファイルパスを渡してFileオブジェクトを作成
		File notExsitFile = new File(path,fileName);

		//exsitメソッドを実行
		if(!notExsitFile.exists()) {
			System.out.println(word + FILE_NOT_EXIST);
			return false;
		}

		BufferedReader br = null;

		try {
			File file = new File(path, fileName);
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			// 一行ずつ読み込む
			while((line = br.readLine()) != null) {
				// ※ここの読み込み処理を変更してください。(処理内容1-2～4)
				String[] items = line.split(",");

				//エラー処理1-2
				if((items.length != 2) || (!(items[0]).matches(express))) {
					System.out.println(word + FILE_INVALID_FORMAT);
					return false;
				}

				names.put(items[0],items[1]);
				sales.put(items[0], 0L);

			}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;

		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> names, Map<String, Long> sales) {

		// ※ここに書き込み処理を作成してください。(処理内容3-1)
		BufferedWriter bw = null;

		try {
			File file = new File(path,fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			for(String key : names.keySet()) {
				bw.write(key + "," + names.get(key) + "," + sales.get(key) );

				bw.newLine();
			}

		}catch (IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;

		}finally {
			if(bw != null) {
				try {
					bw.close();

				}catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

}
